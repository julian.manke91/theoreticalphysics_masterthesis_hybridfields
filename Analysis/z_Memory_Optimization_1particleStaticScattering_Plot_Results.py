import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc, rcParams
from numpy import sin
import csv

#particle 1
x = []
y = []

# particle 2
xTwo = []
yTwo = []

lineWidthRectangles = 3
lineWidthParticles = 1

fig = plt.figure()
ax = plt.gca()


analysisData = np.genfromtxt('retardedTimeAnalysis.txt')

# array starts with particle 1, then particle 0
lengthOfTimeArray = len(analysisData[:,0])
for i in range(0, lengthOfTimeArray / 2):
    #newElement = analysisData[i,0]
    x.append(analysisData[2 * i, 0])
    y.append(analysisData[2 * i, 1])

    xTwo.append(analysisData[(2 * i) + 1, 0])
    yTwo.append(analysisData[(2 * i) + 1, 1])

    # Plots with legend
ax.plot(x, y, 'ro', markersize = 3, label = 'Electron 2')
ax.plot(xTwo, yTwo, 'bo', markersize = 3, label = 'Electron 1')
legend = ax.legend(loc = 'upper right', shadow = True)

filename = 'Analysis'
plt.xlabel('simulation time index')
plt.ylabel('intersection time index')

    # Axis every n-th point
    #inverseTickRate = 1
    #plt.xticks(np.arange(sizeOfOneBox, simulationArea - sizeOfOneBox, sizeOfOneBox))#size of one box
    #plt.yticks(np.arange(0, tN - tPreCalc, inverseTickRate * dt))
    #ax.yaxis.set_ticks_position('left')

    #for index, labels in enumerate(ax.xaxis.get_ticklabels()):
    #    if index % 2 != 0:
    #        labels.set_visible(False)

    # show enumeration of ticks every N-th step
    #inverseLabelEnumRate = 10
    #for index, labels in enumerate(ax.yaxis.get_ticklabels()):
    #    if index % inverseLabelEnumRate != 0:
    #        labels.set_visible(False)


fig.savefig("png/" + "{}.png".format(filename), bbox_inches='tight', dpi = 300)
plt.close(fig)
