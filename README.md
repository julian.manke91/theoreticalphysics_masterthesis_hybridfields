# README # ctr + shift + m for preview

C-Application, currently running in XCode

### What is this repository for? ###

* This is the supposrting code (weitten in C) for my Master-Thesis "Memory Load due to particle history in FDTD simulations" in Computational Physics at the chair of Prof. Hartmut Ruhl at LMU-Munich. 


### How do I get set up? ###

* In order to make this project run on your machine locally: In "Simulation.c" adjust directory path for "BashScript_MakeImagesAndMovie.sh" in "system(...)" to match the path this file is actually stored.
